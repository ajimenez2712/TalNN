class Move(object):

    def __init__(self, initialPosition='', move='', color='w'):
        self.initialPosition = initialPosition
        self.move = move
        self.color = color

    def __str__(self):
        return '\n{%s\n%s}\n' % (self.initialPosition, self.move)

    def __repr__(self):
        return self.__str__()
