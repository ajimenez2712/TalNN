#!/usr/bin/python
# -*- coding: utf-8 -*-

from itertools import izip_longest

from game import Game
from move import Move
import sunfish
import tools

input_file = 'data/Tal-test'
PLAYER_OF_INTEREST = 'Tal, Mihail'
COLOR_BLACK = 'b'
COLOR_WHITE = 'w'

def processTag(line, game):
    if line.startswith('[White '):
        game.whitePlayer = getValueForTag(line)
    elif line.startswith('[Black '):
        game.blackPlayer = getValueForTag(line)
    elif line.startswith('[Result '):
        game.result = getValueForTag(line)

def getValueForTag(line):
    return line.split('"')[1]

def processMoves(line, game):
    moves = []
    splitted_line = (line.replace(' { ', '|').replace(' } ', '|').split('|')[:-1])
    for move, fen in grouper(splitted_line, 2):
        moves.append(Move(tools.parseFEN(fen), move, fen.split()[1]))
    game.moves = moves

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

games = []
game = Game()

with open(input_file, 'r') as f:
    for line in f:
        if line == '\n':
            continue
        if line.startswith('['):
            processTag(line, game)
        else:
            processMoves(line, game)
            games.append(game)
            game = Game()

for game in games:
    color_we_care_for = COLOR_BLACK if (game.whitePlayer == PLAYER_OF_INTEREST) else COLOR_WHITE
    for move in filter(lambda m: m.color == color_we_care_for, game.moves):
        print move.move
        sunfish.print_pos(move.initialPosition)
