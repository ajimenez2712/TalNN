class Game(object):

    RESULT_WHITE_WINS = '1-0'
    RESULT_BLACK_WINS = '0-1'
    RESULT_TIE = '1/2-1/2'

    def __init__(self, whitePlayer='', blackPlayer='', result='', moves=[]):
        self.whitePlayer = whitePlayer
        self.blackPlayer = blackPlayer
        self.result = result
        self.moves = moves

    def __str__(self):
        return '\n{%s - %s\n%s\n%s}\n' % (self.whitePlayer, self.blackPlayer,
                                          self.result, self.moves)

    def __repr__(self):
        return self.__str__()
